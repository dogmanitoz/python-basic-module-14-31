def QHofstadter(start):
    if start != [1, 1]:
        return
    sequence = start[:]
    while 1:
        try:
            x = sequence[-sequence[-1]] + sequence[-sequence[-2]]
            sequence.append(x)
            yield x
        except IndexError:
            StopIteration()


i = 1
for num in QHofstadter([1, 1]):
    if i > 10:
        break
    print(num, end=' ')
    i += 1
