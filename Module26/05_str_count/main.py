import os
from typing import Iterable


def gen_files_dir(path: str, depth=1) -> Iterable[str]:
    depth -= 1
    with os.scandir(path) as p:
        for entry in p:
            yield entry.path
            if entry.is_dir() and depth > 0:
                yield from gen_files_dir(entry.path, depth)


if __name__ == '__main__':
    directory = 'C:\\Test'
    line_count = 0
    files_filtered = [x for x in list(gen_files_dir(directory)) if x.endswith('.py')]

    for file in files_filtered:
        if not os.path.isfile(file):
            continue

        try:
            with open(file, 'r') as object:
                local_count = 0
                for line in object.read().split('\n'):
                    if (not line.strip() == '') and (not line.strip().startswith("#"))\
                                and (not line.strip().startswith('"')):
                        local_count += 1
                print('{} - {} ст'.format(file, local_count))
                line_count += local_count
        except:
            continue

    print("\nВсего строк  - ", line_count)
