class Squares(object):
    def __init__(self, start, stop):
        self.squares = range(start, stop + 1)
        self.stop = stop
        self.count = 0

    def __iter__(self):
        self.count = 0
        return self

    def __next__(self):
        if self.stop > self.count:
            value = self.squares[self.count] ** 2
            self.count += 1
            return value
        else:
            raise StopIteration


if __name__ == '__main__':
    number = int(input('Введите число: '))
    for i in Squares(1, number):
        print(i)


# ----------------------------------


number_2 = int(input('Введите число: '))
i = (x ** 2 for x in range(1, number_2 + 1))
for num in i:
    print(num)


# ----------------------------------


def square_num(num):
    count = 1
    while True:
        if num >= count:
            yield count ** 2
            count += 1
        else:
            break


number_1 = int(input('Введите число: '))
squares = square_num(number_1)
for i_num in squares:
    print(i_num)
