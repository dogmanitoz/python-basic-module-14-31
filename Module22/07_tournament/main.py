import os

first_tour = open('first_tour.txt', 'r')
k = int(first_tour.readline())
with open('first_tour.txt', 'r') as file:
    print(f'Содержимое файла {file.name}:\n{file.read()}')

new_list = []

for line in first_tour:
    new_line = line.split()

    if int(new_line[-1]) > k:
        new_list.append(new_line)
first_tour.close()

new_list.sort(key=lambda a: int(a[-1]))
new_list.reverse()

count = str(len(new_list)) + '\n'

result_tour = []
for i in new_list:
    name_sim = str(i[-2][0]).title() + '. '
    s_win = name_sim + str(i[0]) + ' ' + str(i[-1])
    result_tour.append(s_win + '\n')

with open("second_tour.txt", "w") as second_tour:
    second_tour.write(count)
    for i, value in enumerate(result_tour, start=1):
        second_tour.write(str(i) + ') ')
        second_tour.write(value)

with open("second_tour.txt", "r") as second_tour:
    print(f'\nСодержимое файла {second_tour.name}:')
    print(second_tour.read())
second_tour.close()
