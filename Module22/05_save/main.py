import os


def save_files(string):
    way = input('Куда хотите сохранить документ? Введите последовательность папок (через пробел): ')
    filename = input('Введите имя файла: ')
    r_path = way.replace(" ", "/")
    real_path = os.path.join(r_path, filename)
    path = str('C:/' + real_path)
    check_file = os.path.exists(path)
    if check_file:
        print('Файл с таким именем уже существует!')
        question = input('Вы действительно хотите перезаписать файл? ').lower()
        if question == 'да' or question == 'yes':
            file = open(path, 'w')
            file.write(string)
            file.close()
            print('Файл успешно перезаписан!')
        else:
            print('Файл не перезаписан')
    else:
        file = open(path, 'w')
        file.write(string)
        file.close()
        print('Файл успешно сохранён!')


save_files(input('Введите строку: '))
