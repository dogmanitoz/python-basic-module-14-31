import os
import zipfile

voina_i_mir = zipfile.ZipFile('voyna-i-mir.zip', 'r')
voina_i_mir.extractall('.')

voina_i_mir.close()

text = open('voyna-i-mir.txt', 'r', encoding='utf8')
letters_dict = {}

for i_line in text:
    for i_char in i_line:
        if i_char.isalpha():
            if i_char not in letters_dict:
                letters_dict[i_char] = 1
            else:
                letters_dict[i_char] += 1
            # TODO Можно сделать с помощью метода .get() и значением по умолчанию в одну строку вместо условного оператора
text.close()
sorted_dict = {}
sorted_keys = sorted(letters_dict, key=letters_dict.get, reverse=True)
for i in sorted_keys:
    sorted_dict[i] = letters_dict[i]

with open('letters_in_voina_i_mir.txt', 'w') as file:
    file.write('+{:-^19}+\n'.format('+'))
    file.write('|{: ^9}|{: ^9}|\n'.format('Буква', 'Частота'))
    file.write('+{:-^19}+\n'.format('+'))
    for k, v in sorted_dict.items():
        file.write('|{: ^9}|{: ^9}|\n'.format(k, v))
    file.write('+{:-^19}+\n'.format('+'))
    file.close()
