import os
import string


def caesar_cipher(line, number):
    encrypted_message = ''
    alphabet = [i for i in string.ascii_letters]
    new_message = [(alphabet[(alphabet.index(sym) + number) % 50] if sym != ' ' and sym.isalpha() else ' ')
                   for sym in line]

    for i in new_message:
        encrypted_message += i

    return encrypted_message


with open('text.txt', 'r') as file:
    print(f'Содержимое файла {file.name}:\n', file.read())

with open('text.txt', 'r') as file:
    with open('cipher_text.txt', 'w') as cipher_file:
        shift = 1
        for i_line in file:
            for i_sym in i_line:
                cipher_file.write(caesar_cipher(i_sym, shift))
            shift += 1
            cipher_file.write('\n')

cipher_file = open('cipher_text.txt', 'r')
print(f'Содержимое файла {cipher_file.name}:\n', cipher_file.read())
file.close()
cipher_file.close()
