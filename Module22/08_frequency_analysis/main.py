import os

with open('text.txt', 'r') as file_1:
    print(f'Содержимое файла {file_1.name}')
    print(file_1.read())
    file_1.close()
text = open('text.txt', 'r').read().lower()
letters_dict = {}
count = 0

for i_sym in text:
    if i_sym.isalpha():
        num = letters_dict.get(i_sym, 0)
        letters_dict[i_sym] = num + 1
        count += 1

letters_list = [(k, "{:5.3f}".format(letters_dict[k] / count)) for k in letters_dict.keys()]
letters_list.sort(key=lambda x: x[0])
letters_list.sort(key=lambda x: x[1], reverse=True)
result = "\n".join([i[0] + " " + i[1] for i in letters_list])
open('analysis.txt', "w").write(result)

with open('analysis.txt', 'r') as file_2:
    print(f'\nСодержимое файла {file_2.name}')
    print(file_2.read())
    file_2.close()
