import os
from collections import Counter

zen_file = open(os.path.join('..', '02_zen_of_python', 'zen.txt'))
sum_sym = 0
sum_word = 0
sum_str = 0

for i_line in zen_file:
    # sum_word += (len(list(line.split())))           sum_word
    # line = i_line.split(' ')
    # for i_word in line:              sum_word
    #     sum_word += 1
    sum_word += i_line.count(' ') + 1
    sum_str += 1
    for i_sym in i_line:
        if i_sym.isalpha():
            sum_sym += 1
            # TODO тут можно собрать все буквы без остальных знаков

with open(os.path.join('..', '02_zen_of_python', 'zen.txt')) as file:
    counter = Counter(filter(lambda x: x.isalpha(), file.read().lower()))
    rare_letter = min(counter, key=counter.get)   #не получается исключить символы
    # todo Подсказал выше, но эта тема будет чуть далее по курсу, но можно и в цикле выше просто собрать в строку все
    #  буквы из файла
zen_file.close()

print('Количество букв в файле:', sum_sym)
print('Количество слов в файле:', sum_word)
print('Количество строк в файле:', sum_str)
print('Наиболее редкая буква:', rare_letter)
