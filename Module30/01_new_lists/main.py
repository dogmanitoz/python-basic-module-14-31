from typing import List
from functools import reduce


def my_add(a: int, b: int) -> int:
    result = a * b
    return result


floats: List[float] = [12.3554, 4.02, 5.777, 2.12, 3.13, 4.44, 11.0001]
names: List[str] = ["Vanes", "Alen", "Jana", "William", "Richards", "Joy"]
numbers: List[int] = [22, 33, 10, 6894, 11, 2, 1]

if __name__ == '__main__':
    print(list(map(lambda x: round(x ** 3, 3), floats)))
    print(list(filter(lambda x: len(set(x)) >= 5, names)))
    print(reduce(my_add, numbers))
