print(list(filter(lambda x: x % 2 != 0 and all(map(
    lambda i: x % i != 0, range(3, int(x ** 0.5) + 1, 2))) or x == 2, range(2, 1000 + 1))))

print('\n', '-' * 20, '\n')

result = []
k = 0

for i in range(2, 1000 + 1):
    for j in range(2, i):
        if i % j == 0:
            break
    else:
        result.append(i)

print(result)

print('\n', '-' * 20, '\n')

print([x for x in range(2, 1001) if not [n for n in range(2, x) if not x % n]])
