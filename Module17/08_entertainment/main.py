import random

sticks = int(input('Кол-во палок: '))
shot = int(input('Кол-во бросков: '))

listik = ['I' for i in range(sticks)]

for i in range(1, shot + 1):
    print('Бросок ', i, '. ', sep='', end='')
    f_stick = random.randint(1, sticks)
    s_stick = random.randint(f_stick, sticks)
    print(f'Сбиты палки с номера {f_stick} по номер {s_stick}')
    for j in range(f_stick - 1, s_stick):
        listik[j] = '.'
print()

for i in listik:
    print(i, end='')
