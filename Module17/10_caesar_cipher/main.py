message = input('Введите сообщение: ')
shift = int(input('Введите сдвиг: '))
alphabet = ["а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о",
            "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я"]

encrypted_message = ''

new_message = [(alphabet[(alphabet.index(sym) + shift) % 33] if sym != ' ' else ' ') for sym in message]

for i in new_message:
    encrypted_message += i

print('Зашифрованное сообщение:', encrypted_message)
