text = input('Введите текст: ')
vowels = "ауоиэыяюеё"

vowels_sym = [i for i in text if i in vowels]

print('Список гласных букв:', vowels_sym)
print('Длина списка:', len(vowels_sym))
