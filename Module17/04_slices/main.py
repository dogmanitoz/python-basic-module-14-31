alphabet = 'abcdefg'

#1
new_alphabet = alphabet[:]
print(new_alphabet)
#2
print(alphabet[::-1])
#3
print(alphabet[::2])
#4
print(alphabet[1::2])
#5
print(alphabet[:1])
#6
print(alphabet[:-2:-1])
#7
print(alphabet[3:4])
#8
print(alphabet[-3:])
#9
print(alphabet[3:5])
#10
print(alphabet[4:2:-1])
