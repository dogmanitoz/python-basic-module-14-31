import random

number_count = int(input('Кол-во чисел в списке: '))
list_after_compression = []

list_before_compression = [random.randint(0, 2) for _ in range(number_count)]
print('Список до сжатия:', list_before_compression)

list_after_compression = [i for i in list_before_compression if i]  # + [0]*list_before_compression.count(0)

print('Список после сжатия:', list_after_compression)
