nice_list = [[[1, 2, 3], [4, 5, 6], [7, 8, 9]],
             [[10, 11, 12], [13, 14, 15], [16, 17, 18]]]

outer_list = [number for middle_list in nice_list for inner_list in middle_list for number in inner_list]
print(outer_list)
