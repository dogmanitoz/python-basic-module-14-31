from typing import Callable, Any
from datetime import datetime
import functools


def logging(func: Callable) -> Callable:
    """
    Декоратор, который отвечает за логирование функций
    :param func:
    :return:
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs) -> Any:
        """
        Функция - обертка
        :param args:
        :param kwargs:
        :return:
        """
        try:
            print(f'{func.__name__} - {func.__doc__}')
            func()
        except Exception as error:
            error = f'{datetime.now().strftime("%d.%m.%Y %H:%M:%S")} - {func.__name__} - {error}'
            with open('function_errors.log', 'a+', encoding='utf-8') as file:
                file.write(error + '\n')
            print(error)

    return wrapper


@logging
def test():
    """Функция с ошибкой ZeroDivisionError"""
    x = 1 / 0


@logging
def test_1():
    """Функция с ошибкой NameError"""
    a = 4
    print(a + b)


test()
test_1()
