from typing import Callable, Any
import functools
import time


def sleep(func: Callable) -> Callable:
    """
    Декоратор, который перед выполнением декорируемой функции ждёт несколько секунд
    :param func:
    :return:
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs) -> Any:
        """
        Функция - обертка
        :param args:
        :param kwargs:
        :return:
        """
        time.sleep(3)
        result = func(*args, **kwargs)
        return result

    return wrapper


@sleep
def check_page():
    pass


check_page()
