from typing import Callable, Any
import functools


def stringify_arg(arg: Any) -> str:
    if isinstance(arg, str):
        return f"'{arg}'"
    else:
        return str(arg)


def debug(func: Callable) -> Callable:
    """
    Декоратор, который при каждом вызове декорируемой функции выводит её имя
    (вместе со всеми передаваемыми аргументами)
    :param func:
    :return:
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs) -> Any:
        """
        Функция - обертка
        :param args:
        :param kwargs:
        :return:
        """
        args_str = [stringify_arg(arg) for arg in args]
        kwargs_str = [f'{key}={stringify_arg(value)}' for key, value in kwargs.items()]
        all_args_str = ', '.join(args_str + kwargs_str)
        print('Вызывается {} ({})'.format(func.__name__, all_args_str))
        result = func(*args, **kwargs)
        print("'{}' вернула значение '{}'".format(
            func.__name__, result,
            result
        ))
        print(result + '\n')
        return result

    return wrapper


@debug
def greeting(name, age=None):
    if age:
        return "Ого, {name}! Тебе уже {age} лет, ты быстро растёшь!".format(name=name, age=age)
    else:
        return "Привет, {name}!".format(name=name)


Benjamin = greeting("Том")
greeting("Миша", age=100)
greeting(name="Катя", age=16)
