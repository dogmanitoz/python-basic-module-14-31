from bs4 import BeautifulSoup as bs
import requests


if __name__ == "__main__":
    my_req = requests.get("http://www.columbia.edu/~fdc/sample.html")
    text = my_req.text
    soup = bs(text, "html.parser")
    items = soup.select('h3')

    for item in items:
        print(item.text)
