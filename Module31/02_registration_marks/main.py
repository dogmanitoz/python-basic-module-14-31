import re


numbers = 'А578ВЕ777 ОР233787 К901МН666 СТ46599 СНИ2929П777 666АМР666'

result_private_auto = re.findall(r'[АВЕКМНОРСТУХ]\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\d{2,3}', numbers)
print('Список номеров частных автомобилей:', result_private_auto)

result_taxi = re.findall(r'[АВЕКМНОРСТУХ]{2}\d{3}(?<!000)\d{2,3}', numbers)
print('Список номеров такси:', result_taxi)
