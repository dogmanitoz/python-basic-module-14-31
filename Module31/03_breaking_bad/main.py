import requests
import json


result = requests.get("https://www.breakingbadapi.com/api/deaths")

if result.status_code == 200:
    json_dict = json.loads(result.text)
    sum_death = 0
    for elem in json_dict:
        # TODO предполагался поиск максимального значения смертей
        sum_death = elem['number_of_deaths']
    for elem in json_dict:
        if elem['number_of_deaths'] == sum_death:
            with open('sum_death.json', 'w') as file:
                json.dump(elem, file, indent=4)
    with open('sum_death.json', 'r') as file:
        json_dict = json.load(file)
    for elem in json_dict:
        print(elem, ':', json_dict[elem])
