import re


list_number = ['9999999999', '999999-999', '99999x9999']

pattern = re.compile(r'[8-9][0-9]{9}')

for elem in list_number:
    if re.match(pattern, elem) and len(elem) == 10:
        print(f'{elem} номер соответствует.')
    else:
        print(f'{elem} номер не подходит.')
