from typing import Callable
import functools


def decorator_with_args_for_any_decorator(decorator_to_enhance: Callable) -> Callable:
    """Декоратор для декоратора, дает возможность принимать произвольные аргументы"""
    def decorator_maker(*args, **kwargs) -> Callable:
        def decorator_wrapper(func: Callable) -> Callable:
            return decorator_to_enhance(func, *args, **kwargs)
        return decorator_wrapper

    return decorator_maker


@decorator_with_args_for_any_decorator
def decorated_decorator(func: Callable, *args, **kwargs) -> Callable:
    """Декорируемый декоратор, который декорирует функцию"""
    @functools.wraps(func)
    def wrapper(function_arg1, function_arg2) -> Callable:
        print("Переданные арги и кварги в декоратор::", args, kwargs)
        return func(function_arg1, function_arg2)

    return wrapper


@decorated_decorator(100, 'рублей', 200, 'друзей')
def decorated_function(text: str, num: int) -> None:
    print("Привет,", text, num)


decorated_function("Юзер", 101)
