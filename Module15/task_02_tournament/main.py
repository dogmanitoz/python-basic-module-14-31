def main():
    names_list = ['Артемий', 'Борис', 'Влад', 'Гоша', 'Дима', 'Евгений', 'Женя', 'Захар']
    count = 0
    first_day = []

    for i in names_list:
        if count % 2 == 0:
            first_day.append(i)
        count += 1
    print("Первый день:", first_day)
    pass


if __name__ == '__main__':
    main()
