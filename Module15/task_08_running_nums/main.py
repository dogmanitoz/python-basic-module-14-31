def main():
    # def shift(lst, steps):
    #     if steps < 0:
    #         steps = abs(steps)
    #         for i in range(steps):
    #             lst.append(lst.pop(0))
    #     else:
    #         for i in range(steps):
    #             lst.insert(0, lst.pop())
    #
    #
    # initial_list = []
    # shift_count = int(input("Сдвиг: "))
    #
    # while True:
    #     initial = input("Введите число из списка (для завершения введите - end): ")
    #     if initial == "end":
    #         break
    #     initial_list.append(int(initial))
    # print("Изначальный список:", initial_list)
    #
    # shift(initial_list, shift_count)
    # shifted_list = initial_list
    # print("Сдвинутый список:", shifted_list)

    initial_list = []
    shifted_list = []

    symbol_count = int(input('Kол-во элементов списка: '))
    for _ in range(symbol_count):
        initial_list.append(int(input('Введите элемент: ')))

    shift_count = int(input('Сдвиг: '))
    count = len(initial_list) - shift_count

    for i in range(count, symbol_count):
        shifted_list.append(initial_list[i])
    for i in range(0, count):
        shifted_list.append(initial_list[i])

    print('\nИзначальный список: ', initial_list)
    print('Сдвинутый список: ', shifted_list)
    pass


if __name__ == '__main__':
    main()
