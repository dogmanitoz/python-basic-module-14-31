def main():
    containers = int(input("Кол-во контейнеров: "))
    containers_list = []

    for i in range(containers):
        container_weight = int(input("Введите вес контейнера: "))
        if container_weight > 200:
            print("Вес контайнера не должен превышать 200 кг!")
        else:
            containers_list.append(container_weight)

    new_container = int(input("Введите вес нового контейнера: "))
    count = 0

    for weight in containers_list:
        count += 1
        if weight < new_container:
            containers_list.insert(count - 1, new_container)
            break

    print("Номер, куда встанет новый контейнер:", count)
    print(containers_list)
    pass


if __name__ == '__main__':
    main()
