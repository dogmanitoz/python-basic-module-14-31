def main():
    number = int(input("Введите число: "))
    num_list = []
    for i in range(1, number + 1, 2):
        num_list.append(i)
    print("Список из нечётных чисел от 1 до N:", num_list)
    pass  # TODO эта строка не нужна, её указывают вместо тела фукнции/класса, чтобы код не падал пока нет реализации


if __name__ == '__main__':
    main()
