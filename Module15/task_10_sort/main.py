def main():
    initial_list = []

    while True:
        initial = input("Введите число из списка (для завершения введите - end): ")
        if initial == "end":
            break
        initial_list.append(int(initial))
    print("Изначальный список:", initial_list)

    sorted_list = sorted(initial_list)
    print("Отсортированный список:", sorted_list)
    pass


if __name__ == '__main__':
    main()
