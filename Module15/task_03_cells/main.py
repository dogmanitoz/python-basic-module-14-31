def main():
    cells = int(input("Кол-во клеток: "))
    no_suitable_values = []

    for index in range(cells):
        effectiv = int(input(f'Эффективность {index + 1} клетки: '))
        if effectiv < index:
            no_suitable_values.append(effectiv)

    print("Неподходящие значения:", str(no_suitable_values)[1:-1])

    pass


if __name__ == '__main__':
    main()
