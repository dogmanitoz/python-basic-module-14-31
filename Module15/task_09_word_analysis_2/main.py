def main():
    word = input("Введите слово: ")
    if word == word[::-1]:
        print("Слово является палиндромом")
    else:
        print("Слово не является палиндромом")

    #or

    word = input("Введите слово: ")
    l = len(word)
    for i in range(l//2):
        if word[i] != word[-1 - i]:
            print("Слово не является палиндромом")
            break
    else:
        print("Слово является палиндромом")
    pass


if __name__ == '__main__':
    main()
