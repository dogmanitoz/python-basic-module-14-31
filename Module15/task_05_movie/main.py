def main():
    films = ['Крепкий орешек', 'Назад в будущее', 'Таксист',
             'Леон', 'Богемская рапсодия', 'Город грехов',
             'Мементо', 'Отступники', 'Деревня']
    favorite_films = []

    count = int(input("Сколько фильмов хотите добавить? "))
    for _ in range(count):
        film = input("Введите название фильма: ")
        for name in films:
            if name == film.title():
                favorite_films.append(film.title())
        if film.title() not in films:
            print(f'Ошибка: фильма {film.title()} у нас нет :(')

    print(f'Ваш список любимых фильмов: {favorite_films}')
    pass


if __name__ == '__main__':
    main()
