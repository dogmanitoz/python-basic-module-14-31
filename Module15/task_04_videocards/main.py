def main():
    videocards_count = int(input("Кол-во видеокарт: "))
    old_list = []
    check_list = []
    new_list = []

    for i in range(videocards_count):
        print(f'{i + 1} Видеокарта: ', end='')
        videocard = int(input(""))
        old_list.append(videocard)

    check_list = max(old_list)
    new_list = [i for i in old_list if i != check_list]

    print("Старый список видеокарт:", old_list)
    print("Новый список видеокарт:", new_list)
    pass


if __name__ == '__main__':
    main()
