def main():
    word = input("Введите слово: ")
    letters = list(word)
    unique_letters = 0

    for sym in letters:
        count = 0
        for i in letters:
            if i == sym:
                count += 1
        if count == 1:
            unique_letters += 1

    print("Кол-во уникальных букв:", unique_letters)
    pass


if __name__ == '__main__':
    main()
