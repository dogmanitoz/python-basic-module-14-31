families = {
    'Сидоров Никита': 35,
    'Сидорова Алина': 34,
    'Сидоров Павел': 10,
    'Маяк Антоний': 72,
    'Маяк Агафья': 69
}

surname = input('Введите фамилию: ').lower()
no_result = True

for i_name, i_age in families.items():
    if surname in i_name.split()[0].lower() or surname[:-1] in i_name.split()[0].lower():
        no_result = False
        print(i_name, i_age)

if no_result:
    print('Поиск не дал результатов')
