students = {
    1: {
        'name': 'Bob',
        'surname': 'Vazovski',
        'age': 23,
        'interests': ['biology, swimming']
    },
    2: {
        'name': 'Rob',
        'surname': 'Stepanov',
        'age': 24,
        'interests': ['math', 'computer games', 'running']
    },
    3: {
        'name': 'Alexander',
        'surname': 'Krug',
        'age': 22,
        'interests': ['languages', 'health food']
    }
}


def students_info(students_dict):
    lst = []
    string = ''

    for i in students_dict:
        lst += (students_dict[i]['interests'])
        string += students_dict[i]['surname']
    count = 0
    for _ in string:
        count += 1
    pairs = [
        (i, students_dict[i]['age'])
        for i in students_dict
    ]

    return lst, count, pairs


my_lst = set(students_info(students)[0])
surname_length = students_info(students)[1]
pairs_list = students_info(students)[2]
print('Список пар "ID студента - Возраст":', pairs_list)
print('Полный список интересов всех студентов:', my_lst)
print('Общая длина всех фамилий студентов:', surname_length)
