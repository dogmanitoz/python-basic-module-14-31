records = int(input('Сколько записей вносится в протокол? '))
games_dict = {}
players_list = []

print('Записи (результат и имя):')
for i_record in range(records):
    score, name = input(f'{i_record + 1} запись: ').split(' ')
    players_list.append(name + ': ' + score)  # Строки исходных данных соответствуют строкам протокола и расположены
    # в том же порядке, что и в протоколе
    score = int(score)
    if name in games_dict:
        if score > games_dict[name][0]:
            games_dict[name][0] = score
            games_dict[name][1] = i_record
    else:
        games_dict[name] = [score, i_record]

winner_dict = {}
sorted_keys = dict(sorted(games_dict.items(), key=lambda x: (-x[1][0], x[1][1])))

if len(sorted_keys) < 3:
    print('Соревнования не состоялись, участников меньше 3.')
else:
    for i in sorted_keys:
        winner_dict[i] = games_dict[i]

    print('\nИтоги соревнований:')
    place = 0

    for k, v in winner_dict.items():
        print(f'{place + 1} место. {k} ({v[0]})')
        place += 1
        if place == 3:
            break
