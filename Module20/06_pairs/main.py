origin_list = [i for i in range(10)]
print('Оригинальный список:', origin_list)

list_dict = {}
new_list = []

for i in origin_list[0::2]:
    list_dict.update({i: i + 1})

for k, v in list_dict.items():
    new_list.append((k, v))

print('Новый список:', new_list)

# or

second_list_dict = {}
for i_index, i_value in enumerate(origin_list):
    if i_index % 2 == 0:
        second_list_dict[i_index] = i_value
print(second_list_dict)
result = [(i_key, i_value + 1) for i_key, i_value in second_list_dict.items()]
print(f'Новый список: {result}')
