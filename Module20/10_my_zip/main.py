def short_len(str, tpl):
    return min(len(str), len(tpl))


string = 'abcd'
tuple_number = (10, 20, 30, 40)

result = ((string[i_sym], tuple_number[i_sym])
          for i_sym in range(short_len(string, tuple_number)))

print('Результат:\n', result)
for i in result:
    print(i)

print(zip(string, tuple_number))

# or (решение не мое)


def my_zip(*arg):
    arg = list(map(list, arg))
    try:
        yield tuple(map(lambda x: x.pop(0), arg))
        yield from my_zip(*arg)
    except:
        pass


for a in my_zip('abcd', (10, 20, 30, 40),):
    print(a)
