def tpl_sort(sort_list):
    for number in sort_list:
        if type(number) is not int:
            return sort_list

    return tuple(sorted(sort_list))


print(tpl_sort((6, 3, -1, 8.5, 4, 10, -5)))
