def check_fio(phonebook, user):
    for k, v in phonebook.items():
        if user in k:
            return f'{k} {v}'


phone_dict = {}

while True:
    action = int(input('Введите номер действия:\n1. Добавить контакт\n2. Найти человека\n'))

    if action == 1:
        print('\nТекущая книга контактов:')
        for k, v in phone_dict.items():
            print(f'{k}: {v}')
        fio = input('Введите имя и фамилию нового контакта (через пробел): ')
        if fio in phone_dict:
            print('Ошибка: такое имя уже существует.')
            continue
        else:
            phone_number = int(input('Введите номер телефона: '))
            phone_dict[fio] = phone_number
    elif action == 2:
        surname_found = input('Введите фамилию для поиска: ').lower()
        result = check_fio(phone_dict, surname_found)
        print(f'Информация по контакту {surname_found}:')
        if result is None:
            print('Такой фамилии нет в списке')
        else:
            print(result)
            continue
