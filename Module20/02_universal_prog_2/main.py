def crypto(checking_list):
    return [i for i in checking_list if is_prime(checking_list.index(i))]


def is_prime(n):
    if n < 2:
        return False
    for i in range(2, int(n ** 0.5 + 1)):
        if n % i == 0:
            return False
    else:
        return True


print(crypto([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
print(crypto('О Дивный Новый мир!'))  # есть ошибка, нужна помощь.
