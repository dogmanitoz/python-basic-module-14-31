def function(f_string, s_string):
    step = 1
    test = False
    while step <= len(s_string):
        new_string = f_string[-step:] + f_string[:-step]
        if new_string == s_string:
            test = True
            break
        step += 1
    if test:
        print(f'Первая строка получается из второй со сдвигом {step}')
    else:
        print(f'Первую строку нельзя получить из второй с помощью циклического сдвига.')


first_string = input('Первая строка: ')
second_string = input('Вторая строка: ')

function(first_string, second_string)

