while True:
    password = input('Придумайте пароль: ')

    length = len(password)
    capital_letters = len(list(filter(lambda x: x.isupper(), password)))
    numbers = len(list(filter(lambda x: x.isdigit(), password)))

    if (length >= 8) and (numbers >= 3) and (capital_letters >= 1):
        print('Это надёжный пароль!')
        break
    else:
        print('Пароль ненадёжный. Попробуйте ещё раз.')
