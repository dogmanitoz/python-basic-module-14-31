def get_encrypted_word(message, key):
    translated_step_1 = ''
    translated_step_2 = ''
    for symbol in message:
        if symbol in letters:
            num = letters.find(symbol)
            translated_step_1 += letters[num-key]
        else:
            translated_step_1 += symbol
    replace_index = 3
    for word in translated_step_1.split(' '):
        new_word = ''
        for index in range(len(word)):
            new_word += (word[index - replace_index % len(word)])
        if new_word.endswith('/'):
            replace_index += 1
        translated_step_2 += new_word + ' '
    translated_step_2 = translated_step_2.replace('/', '\n')
    return translated_step_2


letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

crypted_string = input('Введите зашифрованное сообщение: ')

print(get_encrypted_word(crypted_string, 1))

# TODO осталось ещё заменить:
#  1) знак ( на апостроф
#  2) знаки + на *
#  3) .. на --
