def message(m, number):
    if m == 0:
        return "IP-адрес корректен"
    if m == 1:
        return number + " не целое число"
    if m == 2:
        return number + " больше 255"
    return "Адрес - это четыре числа, разделённые точками"


def fnk(t):
    count_ipaddress = 1
    if not t.isdigit():
        return count_ipaddress
    count_ipaddress = 2
    if int(t) > 255:
        return count_ipaddress
    return 0


ipaddress = input('Введите IP: ').split('.')
count_ipaddress = len(ipaddress)
string = ''
count = 0

for string in ipaddress:
    if count_ipaddress != 4 and count == 0:
        break
    count += 1
    count_ipaddress = fnk(string)
    if count_ipaddress != 0:
        break

print(message(count_ipaddress, string))
