string = input('Введите строку: ')
new_string = []
count = 1

for i in range(len(string) - 1):
    if string[i] == string[i + 1]:
        count += 1
    if string[i] != string[i + 1] or i == len(string) - 2:
        new_string += string[i] + str(count)
        count = 1

if string[-2] != string[-1]:
    new_string += string[-1] + '1'
encoded_string = ''.join(new_string)

print('Закодированная строка:', encoded_string)
