name_file = input('Название файла: ')
special_sym = '@№$%^&*()'

if not name_file.endswith(('.txt', '.docx')):
    print('Ошибка: неверное расширение файла. Ожидалось .txt или .docx')
elif name_file[0] in special_sym:
    print("Ошибка: название начинается на один из специальных символов")
else:
    print("Файл назван верно.")
