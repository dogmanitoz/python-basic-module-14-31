import random


class BlackJack:
    def __init__(self):
        self.deck = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King', 'Ace'] * 4
        self.score = 0
        self.pc_score = 0

    def print_card(self, current, score, pc):
        if not pc:
            print(f'Вам попалась карта {current}. У вас {score} очков.')
        else:
            print(f'Крупье попалась карта {current}. У крупье {score} очков')

    def random_card(self, score, pc):
        current = self.deck.pop()
        if type(current) is int:
            score += current
        elif current == 'Ace':
            if score <= 10:
                score += 11
            else:
                score += 1
        else:
            score += 10
        self.print_card(current, score, pc)
        return score

    def choice(self):
        score = self.random_card(self.score, False)
        pc_score = self.random_card(self.pc_score, True)
        while True:
            choice = input('Будете брать карту? y/n\n')
            if choice == 'y':
                score = self.random_card(score, False)
                if pc_score < 19 and score <= 21:
                    pc_score = self.random_card(pc_score, True)
                if score > 21 or pc_score == 21:
                    print('Извините, но вы проиграли')
                    break
                elif score == 21 and pc_score == 21:
                    print('ничья')
                elif score == 21 or pc_score > 21:
                    print('Поздравляю, вы победили!')
                    break
            elif choice == 'n':
                if score > pc_score and pc_score < 19:
                    while pc_score < 19:
                        pc_score = self.random_card(pc_score, True)
                if score < pc_score <= 21:
                    print(f'Вы проиграли, у вас {score} очков, у крупье {pc_score} очков')
                else:
                    print(f'Вы победили, у вас {score} очков, у крупье {pc_score} очков')
                break

    def start(self):
        random.shuffle(self.deck)
        print('Игра в BlackJack началась')
        print('В блэкджеке десятки, валеты, дамы и короли стоят по 10 очков.\n'
              'Туз может стоить 1 или 11 очков(Обычно считается за 11, '
              'но если при подсчете сумма очков в вашей руке превышает 21, то туз считается за 1)')
        print('----------------------------------')
        self.choice()

        print('До новых встреч!')


while True:
    game = BlackJack()
    game.start()
    print()
