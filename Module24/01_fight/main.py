import random


class Unit:

    def __init__(self, name, health=100):
        self.name = name
        self.health = health

    def hit(self, target):
        if type(self) == type(target):
            target.health -= 20
        else:
            raise TypeError


units = [Unit('Воин 1'), Unit('Воин 2')]
print('Началась битва 2х войнов.\n')

while True:
    random.shuffle(units)
    assault, victim = units[0], units[1]
    assault.hit(victim)
    print('{} атаковал {}'.format(assault.name, victim.name))
    print('У {} осталось {} здоровья'.format(victim.name, victim.health))
    if victim.health == 0:
        print('{} победил.'.format(assault.name))
        break
