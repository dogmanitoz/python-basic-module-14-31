from random import randint, choice


class Person:
    def __init__(self, name):
        self.name = name
        self.satiety = 50

    def eat(self):
        if House.fridge > 0:
            self.satiety += 1
            House.fridge -= 1
            return '{} ест. Сытость {} еды в холодильнике {}'.format(self.name, self.satiety, House.fridge)
        else:
            return 'Еды в доме нет!'

    def work(self):
        self.satiety -= 1
        House.table_money += 1
        return '{} работает. Сытость {}, денег в тумбочке - {}'.format(self.name, self.satiety, House.table_money)

    def play(self):
        self.satiety -= 1
        return '{} играет. Сытость {}'.format(self.name, self.satiety)

    def shopping(self):
        if House.table_money > 0:
            House.fridge += 1
            House.table_money -= 1
            return '{} пошел в магазин. Еды в холодильнике - {}, денег в тумбочке - {}'\
                .format(self.name, House.fridge, House.table_money)
        else:
            return 'Денег в доме нет!'


class House:
    fridge = 50
    table_money = 0


day = 1
man_1 = Person('Артем')
man_2 = Person('Андрей')

for i in range(day, 365 + 1):
    print('День {}й.'.format(day))
    cub_number = randint(1, 6)
    man = choice([man_1, man_2])
    if man_1.satiety < 0:
        print('К сожалению, {} умер от голода.'.format(man_1.name))
        break
    if man.satiety < 20:
        action = man.eat()
    elif House.fridge < 10:
        action = man.shopping()
    elif House.table_money < 50:
        action = man.work()
    elif cub_number == 1:
        action = man.work()
    elif cub_number == 2:
        action = man.eat()
    else:
        action = man.play()
    print(action)
    day += 1

print('Эксперимент прошел удачно. Они прожили целый год.' if day == 365 + 1 else 'Эксперимент провалился.')
