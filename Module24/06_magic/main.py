class Element:

    def __init__(self, n):
        if n in ('Вода', 'Воздух', 'Огонь', 'Земля', 'Дерево', 'Металл',
                 'Шторм', 'Грязь', 'Пыль', 'Лава', 'Молния', 'Пар', 'Ржавчина', 'Уголь'):
            self.name = n
        else:
            raise ValueError

    def __add__(self, other):
        if (self.name, other.name) == ('Вода', 'Воздух'):
            return Element('Шторм')
        if (self.name, other.name) == ('Вода', 'Огонь'):
            return Element('Пар')
        if (self.name, other.name) == ('Вода', 'Земля'):
            return Element('Грязь')
        if (self.name, other.name) == ('Воздух', 'Огонь'):
            return Element('Молния')
        if (self.name, other.name) == ('Воздух', 'Земля'):
            return Element('Пыль')
        if (self.name, other.name, ) == ('Огонь', 'Земля'):
            return Element('Лава')
        if (self.name, other.name) == ('Вода', 'Металл'):
            return Element('Ржавчина')
        if (self.name, other.name, ) == ('Огонь', 'Дерево'):
            return Element('Уголь')
        raise ValueError


try:
    element_1 = Element('Вода')
    element_2 = Element('Металл')
    new_element = element_1 + element_2
    print('{} + {} = {}'.format(element_1.name, element_2.name, new_element.name))
except ValueError:
    print(None)


# or


class Air:
    name = 'Воздух'

    def __str__(self):
        return Air.name

    def __add__(self, other):
        if isinstance(other, Water):
            return Storm()
        elif isinstance(other, Fire):
            return Lightning()
        elif isinstance(other, Earth):
            return Dust()
        else:
            return None


class Water:
    name = 'Вода'

    def __str__(self):
        return Water.name

    def __add__(self, other):
        if isinstance(other, Fire):
            return Steam()
        elif isinstance(other, Earth):
            return Dirt()
        elif isinstance(other, Air):
            return Storm()
        elif isinstance(other, Metal):
            return Rust()
        else:
            return None


class Fire:
    name = 'Огонь'

    def __str__(self):
        return Fire.name

    def __add__(self, other):
        if isinstance(other, Water):
            return Steam()
        elif isinstance(other, Earth):
            return Lava()
        elif isinstance(other, Air):
            return Lightning()
        elif isinstance(other, Wood):
            return Coal()
        else:
            return None


class Earth:
    name = 'Земля'

    def __str__(self):
        return Earth.name

    def __add__(self, other):
        if isinstance(other, Water):
            return Dirt()
        elif isinstance(other, Fire):
            return Lava()
        elif isinstance(other, Air):
            return Dust()
        else:
            return None


class Steam:
    def __str__(self):
        return 'Пар'


class Dust:
    def __str__(self):
        return 'Пыль'


class Storm:
    def __str__(self):
        return 'Шторм'


class Lightning:
    def __str__(self):
        return 'Молния'


class Dirt:
    def __str__(self):
        return 'Грязь'


class Lava:
    def __str__(self):
        return 'Лава'


class Coal:
    def __str__(self):
        return 'Уголь'


class Metal:

    def __str__(self):
        return 'Металл'

    def __add__(self, other):
        if isinstance(other, Water):
            return Rust()
        else:
            return None


class Wood:

    def __str__(self):
        return 'Дерево'

    def __add__(self, other):
        if isinstance(other, Fire):
            return Coal()
        else:
            return None


class Rust:
    def __str__(self):
        return 'Ржавчина'


def main():
    air = Air()
    earth = Earth()
    fire = Fire()
    water = Water()
    metal = Metal()
    wood = Wood()
    print(f'{water} + {metal} = {water + metal}')
    print(f'{wood} + {fire} = {fire + wood}')


if __name__ == '__main__':
    main()
