import math


class Circle:

    def __init__(self, x=0, y=0, r=1):
        self.x = x
        self.y = y
        self.r = r

    def square(self):
        return self.r ** 2 * math.pi

    def perimetr(self):
        return 2 * self.r * math.pi

    def grow(self, k):
        self.r *= k

    def cross(self, other):
        return (self.x - other.x) ** 2 + (self.y - other.y) ** 2 <= (self.r + other.r) ** 2
