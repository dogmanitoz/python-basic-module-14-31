class Student:

    def __init__(self, full_name, group_number, marks):
        self.full_name = full_name
        self.group_number = group_number
        self.marks = marks

    def give_average(self):
        average = sum(self.marks) / len(self.marks)
        return average

    def __str__(self):
        return f'{self.full_name} {self.group_number} {self.marks}'


def receiving_data():
    name = input('Введите ФИ: ')
    group = input('Введите номер группы: ')
    marks = list(map(int, input('Введите оценки через пробел: ').split()))
    return name, group, marks


student = Student
list_student = [Student(*receiving_data()) for _ in range(3)]

list_student.sort(key=lambda x: x.give_average(), reverse=True)
print('Список студентов отсортированный по среднему баллу:')
for student in list_student:
    print(student)
