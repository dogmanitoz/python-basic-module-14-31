class Parent:
    def __init__(self, name, age, babys):
        self.name, self.age, self.babys = name, age, babys

    def print_info(self):
        print('Меня зовут {}. Мне {} лет. Моих детей зовут '.format(
            self.name, self.age) + ' и '.join([str(i.name) for i in self.babys]))

    def calm(self, baby):
        for i in self.babys:
            if i is baby:
                i.condition = True

    def feed(self, baby):
        for i in self.babys:
            if i is baby:
                i.is_hunger = True


class Baby:
    def __init__(self, name, age, condition, is_hunger):
        self.name, self.age, self.condition, self.is_hunger = name, age, condition, is_hunger

    def __str__(self):
        return self.name + ' ' + str(self.age) + ' лет ' + \
               ('Спокоен ' if self.condition else 'Раздражен ') + \
               ('Сыт' if self.is_hunger else 'Голоден')


oleg = Baby('Олег', 5, False, False)
irina = Baby('Ирина', 10, False, False)
father = Parent('Иван', 35, [oleg, irina])
father.print_info()
print(oleg, irina)
while oleg.condition is False or oleg.is_hunger is False\
        or irina.condition is False or irina.is_hunger is False:
    father.calm(oleg)
    father.feed(oleg)
    father.calm(irina)
    father.feed(irina)
print(oleg, irina)
