def my_zip(*args):
    arg = list(map(list, args))
    try:
        yield tuple(map(lambda x: x.pop(0), arg))
        yield from my_zip(*args)
    except:
        pass


a = [{'x': 4}, 'b', 'z', 'd']
b = (10, {20, }, [30], 'z')
for i in (my_zip(a, b)):
    print(i)


a = [1, 2, 3, 4, 5]
b = {1: 's', 2: 'q', 3: 4}
x = (1, 2, 3, 4, 5)
for i in (my_zip(a, b, x)):
    print(i)
