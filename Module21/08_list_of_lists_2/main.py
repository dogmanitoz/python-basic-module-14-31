def disclose(work_list, final_list=[]):
    for i_sym in work_list:
        if isinstance(i_sym, list):
            disclose(i_sym)
        else:
            final_list.append(i_sym)
    return final_list


nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]],
             [[11, 12, 13], [14, 15], [16, 17, 18]]]

print(disclose(nice_list))
