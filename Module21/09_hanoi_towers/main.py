def move(n, x=1, y=3):
    if n == 1:
        print(f'Переложит диск {n} со стержня номер {x} на стержень номер {y}')
    else:
        move(n - 1, x, 6 - x - y)
        print(f'Переложит диск {n} со стержня номер {x} на стержень номер {y}')
        move(n - 1, 6 - x - y, y)


disk_count = int(input('Введите количество дисков: '))
move(disk_count)
