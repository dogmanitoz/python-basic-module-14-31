site = {
    'html': {
        'head': {
            'title': 'Мой сайт'
        },
        'body': {
            'h2': 'Здесь будет мой заголовок',
            'div': 'Тут, наверное, какой-то блок',
            'p': 'А вот здесь новый абзац'
        }
    }
}


def find_key(struct, key, count):
    if key in struct:
        return struct[key]

    if count > 1:
        for sub_struct in struct.values():
            if isinstance(sub_struct, dict):
                result = find_key(sub_struct, key, deep - 1)
                if result:
                    break
        else:
            result = None

        return result


def reply():
    value = find_key(site, user_key, deep)
    if value:
        print('Значение ключа:', value)
    else:
        print('Такого ключа в структуре сайта нет. Значение ключа:', value)


user_key = input('Введите искомый ключ: ')
answer = input('Хотите ввести максимальную глубину? Y/N: ').lower()
if answer == 'n':
    deep = 0
    reply()
elif answer == 'y':
    deep = int(input('Введите максимальную глубину: '))
    reply()
else:
    print('Ошибка. Неизвестный параметр.')
