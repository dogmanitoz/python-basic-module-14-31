def summ(*args):
    def flatten(i_list):
        result = []
        for j in i_list:
            if isinstance(j, int):
                result.append(j)
            else:
                result.extend(flatten(j))
        return result
    return sum(flatten(args))


print(summ([[1, 2, [3]], [1], 3]))
print(summ(1, 2, 3, 4, 5))
