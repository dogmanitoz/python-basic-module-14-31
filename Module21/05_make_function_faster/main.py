def calculating_math_func(data, cache={0: 1}):
    if data not in cache:
        num = max(cache)
        for i in range(num + 1, data + 1):
            cache[i] = cache[i - 1] * i
    return pow(cache[data] / pow(data, 3), 10)


print(calculating_math_func(int(input('Введите число: '))))
