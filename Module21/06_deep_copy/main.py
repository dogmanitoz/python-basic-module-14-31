site = {
    'html': {
        'head': {
            'title': 'Куплю/продам телефон недорого'
        },
        'body': {
            'h2': 'У нас самая низкая цена на iphone',
            'div': 'Купить',
            'p': 'продать'
        }
    }
}


def find_key(struct, key, info):
    if key in struct:
        struct[key] = info
        return site

    for sub_struct in struct.values():
        if isinstance(sub_struct, dict):
            result = find_key(sub_struct, key, info)
            if result:
                return site


sites_count = int(input('Сколько сайтов: '))
for _ in range(sites_count):
    sites_name = input('Введите название продукта для нового сайта: ')
    key = {'title': f'Куплю/продам {sites_name} недорого', 'h2': f'У нас самая низкая цена на {sites_count}'}
    for i in key:
        find_key(site, i, key[i])

    print(f'Сайт для {sites_name}:')
    print(site, '\n')

print(site)   # TODO Как видите переменная site изменилась, что запрещено заданием, поэтому надо составлять список из
              #  изменённых "глубоких копий" переменой site (copy.deepcopy)
