import math
from random import randint


class Auto:
    """Класс описывающий автомобиль."""
    def __init__(self, x, y, angle):
        self.x = x
        self.y = y
        self.angle = angle

    def move(self, dist):
        self.x = self.x + dist * math.cos(self.angle)
        self.y = self.y + dist * math.sin(self.angle)


class Bus(Auto):
    """Класс описывающий автобус"""
    def __init__(self, x, y, angle, passenger_count):
        super().__init__(x, y, angle)
        self.passenger_count = passenger_count
        self.incoming_pass = 0
        self.cash = 0

    def move(self, dist):
        super().move(dist)
        if randint(1, 2) == 1:
            if self.passenger_count >= 50:
                self.come_out()
        else:
            if self.passenger_count <= 50:
                self.come_in()

    def come_in(self):
        self.incoming_pass += randint(0, 5)
        self.passenger_count += self.incoming_pass
        self.cash = self.incoming_pass * 25
        self.incoming_pass = 0

    def come_out(self):
        self.passenger_count -= randint(0, 5)


if __name__ == '__main__':
    auto = Auto
    bus = Bus
