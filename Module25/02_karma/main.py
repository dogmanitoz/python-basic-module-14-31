from random import randint, choice


class KillError:
    """Класс исключения"""
    def __str__(self):
        return f'Я принимаю правило учения воздерживаться от убийства живых существ'


class DrunkError:
    """Класс исключения"""
    def __str__(self):
        return f'Я принимаю правило учения воздерживаться от напитков и средств, вызывающих помутнение сознания.'


class CarCrashError:
    """Класс исключения"""
    def __str__(self):
        return f'Я принимаю правило учения воздерживаться от прелюбодеяния.'


class GluttonyError:
    """Класс исключения"""
    def __str__(self):
        return f'Я принимаю правило учения воздерживаться от взятия того, что мне не было дано.'


class DepressionError:
    """Класс исключения"""
    def __str__(self):
        return f'Я принимаю правило учения воздерживаться от неправдивых слов.'


class Karma:
    """Класс кармы"""
    def __init__(self, karma=0):
        self.__karma = karma

    def get_karma(self):
        """Геттер для получения кол-ва кармы"""
        return self.__karma

    def set_karma(self, light):
        """Сеттер для увеличения кармы"""
        self.__karma += light

    def one_day(self, count):
        """Метод с вероятностью 10% вызывает одно из исключений, записывает в karma.log и не отправляет ничего
        или возвращает увеличение кармы на рандомное число от 1 до 7"""
        if randint(1, 10) == 5:
            with open('karma.log', 'a', encoding='utf-8') as karma_file:
                exclusion = choice([KillError(), DrunkError(), CarCrashError(), GluttonyError(), DepressionError()])
                karma_file.write(f'День {count}: исключение - {exclusion}\n')
                return False
        else:
            return randint(1, 7)


day = 0
karma = Karma()
while karma.get_karma() < 500:
    day += 1
    if karma.one_day(day):
        karma.set_karma(karma.one_day(day))
print('Буддист достиг просветления.')

