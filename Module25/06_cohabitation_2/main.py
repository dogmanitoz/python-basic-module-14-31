from random import randint


class House:
    """
    Базовый класс, описывающий дом

    Args:
        family (list): список всех персонажей дома.

    Attributes:
        cash (int): количеством денег в тумбочке (вначале 100)
        food (int): количеством еды в холодильнике (вначале 50)
        cat food (int): еда для кота (вначале 30)
        dirt (int): количество грязи в доме(вначале 0)
        fur coat (int): количество купленных шуб
        food eaten (int): количество съеденной еды за год
    """
    cash = 100
    food = 50
    cat_food = 30
    dirt = 0
    fur_coat = 0
    food_eaten = 0

    def __init__(self, family):
        self.family = family

    def living(self):
        House.dirt += 5
        for i_person in self.family:
            if House.dirt >= 90 and not isinstance(i_person, Cat):
                i_person.happy -= 10
            if isinstance(i_person, Women) and women.happy <= 50:
                women.pet_cat()
            if isinstance(i_person, Man) and man.happy <= 50:
                man.pet_cat()

            if isinstance(i_person, Cat):
                if cat.satiety <= 30 and house.cat_food >= 10:
                    cat.eat()
                elif randint(1, 5) == 3:
                    cat.tear()
                else:
                    cat.sleep()

            if isinstance(i_person, Man):
                if man.satiety <= 30:
                    man.eat()
                elif man.happy <= 30:
                    man.play()
                else:
                    man.work()

            if isinstance(i_person, Women):
                if women.satiety <= 30:
                    women.eat()
                elif House.food <= 30 or House.cat_food <= 30 and House.cash >= 50:
                    women.buy_food()
                elif House.dirt > 30:
                    women.cleaning()
                else:
                    women.shopping()


class Person:
    """Базовый класс для всех персонажей."""
    def __init__(self, name, satiety=30, happy=None):
        self.name = name
        self.satiety = satiety
        self.happy = happy

    def pet_cat(self):
        self.satiety -= 10
        self.happy += 5
        print('{} погладил(а) кота. Все лежат довольные.'.format(self.name))

    def eat(self):
        if House.food > 0:
            self.satiety += 30
            House.food -= 30
            House.food_eaten += 30
            print('{} поел(а).'.format(self.name))


class Man(Person):
    """Класс, описывающий мужа."""
    def __init__(self, name):
        super().__init__(name, happy=100)

    def play(self):
        self.satiety -= 10
        self.happy += 20
        print('{} играет в PS5 и пьет пиво.'.format(self.name))

    def work(self):
        House.cash += 150
        self.satiety -= 10
        print('{} сегодня работает и зарабатывает для всех деньги.'.format(self.name))


class Women(Person):
    """Класс, описывающий жену"""
    def __init__(self, name):
        super().__init__(name, happy=100)

    def shopping(self):
        if House.cash >= 350:
            self.satiety -= 10
            self.happy += 60
            House.fur_coat += 1
            House.cash -= 350
            print('{} пошла покупать себе очередную шубу.'.format(self.name))

    def buy_food(self):
        if House.cash >= 60:
            self.satiety -= 10
            House.food += 30
            House.cash -= 60
            House.cat_food += 30
            print('{} пошла покупать еду для дома и для кота {}.'.format(self.name, cat.name))

    def cleaning(self):
        self.satiety -= 10
        House.dirt -= 100
        print('Дома стало грязно. {} моет и убирает квартиру'.format(self.name))


class Cat(Person):
    """Класс, описывающий кота."""
    def __init__(self, name):
        super().__init__(name)

    def sleep(self):
        self.satiety -= 10
        print('Кот {} спит.'.format(cat.name))

    def tear(self):
        self.satiety -= 10
        House.dirt += 5
        print('Кот {} дерет обои.'.format(cat.name))

    def eat(self):
        self.satiety += 20
        House.cat_food -= 10
        print('Кот {} поел и доволен.'.format(cat.name))


def dead(person_list):
    for i_person in person_list:
        if i_person.satiety <= 0:
            print(f'{i_person.name} умер от голода')
            return True
        elif i_person.happy < 10 and not isinstance(i_person, Cat):
            print(f'{i_person.name} умер от депрессии.')
            return True
        else:
            return False


man = Man('Менелай')
women = Women('Елена')
cat = Cat('Гамильтон')
family = [man, women, cat]
house = House(family)

for i_day in range(1, 366):
    print('\nДень {}й.'.format(i_day))
    if dead(family):
        print('Эксперимент прошел неудачно.')
        break
    elif i_day == 365:
        house.living()
        print('\nЭксперимент прошел удачно. Семья продолжает жить.')
        print('\nЗа прошедший год:'
              '\nДенег в доме:{}'.format(House.cash),
              '\nСъедено еды: {}'.format(House.food_eaten),
              '\nКуплено шуб: {}'.format(House.fur_coat))
        break
    else:
        house.living()
