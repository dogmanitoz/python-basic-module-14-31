class Property:
    """
    Базовый класс имуществва налогоплатильщика

    worth = стоимость имущества
    """
    def __init__(self, worth):
        self.worth = worth

    def tax(self):
        pass


class Apartment(Property):
    """Класс описывает квартиру налогоплатильщика"""
    def __init__(self, worth):
        super().__init__(worth)

    def tax(self):
        return self.worth / 1000


class Car(Property):
    """Класс описывает машину налогоплатильщика"""
    def __init__(self, worth):
        super().__init__(worth)

    def tax(self):
        return self.worth / 200


class CountryHouse(Property):
    """Класс описывает дачу налогоплатильщика"""
    def __init__(self, worth):
        super().__init__(worth)

    def tax(self):
        return self.worth / 500


print(' ***** Расчет налогов на имущество *****\n')
cash = int(input('Введите количество имеющихся у Вас денег: '))

apartment_price = float(input('Введите стоимость квартиры: '))
apartment = Apartment(apartment_price)
print('Налог на квартиру - {}'.format(apartment.tax()))

car_price = float(input('Введите стоимость машины: '))
car = Car(car_price)
print('Налог на машину: {}'.format(car.tax()))

country_house_price = float(input('Введите стоимость дачи: '))
country_house = CountryHouse(country_house_price)
print('Налог на дачу: {}'.format(country_house.tax()))

total_tax = country_house.tax() + car.tax() + apartment.tax()
if cash > total_tax:
    print('\nУ Вас достаточно денег на оплату всех налогов.\nСумма всех налогов {}'.format(total_tax))
else:
    shortage = total_tax - cash
    print('\nСумма всех налогов {}.\nДля оплаты все налогов у Вас не хватает {}'.format(total_tax, shortage))
