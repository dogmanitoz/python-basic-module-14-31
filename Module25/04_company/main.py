from random import randint, choice


class Person:
    """
    Класс, который описывает человека именем, фамилией и возрастом.
    Все атрибуты класса приватны.
    """
    def __init__(self, name, surname, age):
        self.__name = name
        self.__surname = surname
        self.__age = age

    def __str__(self):
        return f'Меня зовут {self.__name} {self.__surname}. Мой возраст {self.__age}'


class Employee(Person):
    """Основной класс работника"""
    def calc_salary(self):
        pass

    def __str__(self):
        return super().__str__() + f'\nМоя зарплата {self.calc_salary()}\n'


class Manager(Employee):
    """Класс работника Manager"""
    def calc_salary(self):
        return 13000


class Agent(Employee):
    """Класс работника Agent"""
    sales: int

    def calc_salary(self):
        return 5000 + 0.5 * self.sales


class Worker(Employee):
    """Класс работника Worker"""
    hours: int

    def calc_salary(self):
        return 100 * self.hours


workers = list()
names = ['Олег', 'Андрей', 'Максим', 'Иван', 'Антон', 'Алексей', 'Борис', 'Владимир', 'Константин']
surnames = ['Вещий', 'Самовалов', 'Наливайко', 'Пупкин', 'Дубкин', 'Воевода', 'Небритый', 'Долгоногий', 'Цзю']
age_count = randint(18, 60)

for _ in range(3):
    workers.append(Manager(name=choice(names), surname=choice(surnames), age=age_count))

for _ in range(3):
    agent = Agent(name=choice(names), surname=choice(surnames), age=age_count)
    agent.sales = randint(1000, 10000)
    workers.append(agent)

for _ in range(3):
    worker = Worker(name=choice(names), surname=choice(surnames), age=age_count)
    worker.hours = randint(150, 250)
    workers.append(worker)

for worker in workers:
    print(worker)
