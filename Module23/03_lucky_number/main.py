import random

total_sum = 0

try:
    with open('out_file.txt', 'a') as file:
        while True:
            if total_sum < 777:
                number = int(input('Введите число: '))
                total_sum += number
                if random.choices((0, 1), (1-1/13, 1/13))[0]:
                    raise ValueError
                else:
                    file.write(f'{number}\n')
            else:
                print('Вы успешно выполнили условие для выхода из порочного цикла!')
                break
except ValueError:
    print('Вас постигла неудача!')
finally:
    with open('out_file.txt', 'r') as file:
        print(f'\nСодержимое файла {file.name}:')
        for i in file:
            print(i, end='')
