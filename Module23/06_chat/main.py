user_name = input('Введите имя: ')

while True:
    print('Чтобы посмотреть текущий текст чата - введите 0')
    print('Чтобы отправить сообщение - введите 1')
    action = int(input())

    if action == 0:
        try:
            with open('chat.txt', 'r', encoding='utf-8') as file:
                for message in file:
                    print(message, end='')
        except FileNotFoundError:
            print('Нет истории сообщений')

    elif action == 1:
            message = input('Введите сообщение: ')
            with open('chat.txt', 'a', encoding='utf-8') as file:
                file.write(f'{user_name}: {message}\n')
    else:
        print('Неизвестная команда. Попробуйте снова.')
