import os


def verification_data(file):
    name, email, age = file.split(' ')
    sym_email = ('@', '.')
    age = int(age)
    if len(file.split(' ')) != 3:
        raise IndexError(' НЕ присутствуют все три поля' + '\n')
    elif name.isalpha() is False:
        raise NameError(' Поле «Имя» содержит НЕ только буквы' + '\n')
    elif age not in range(10, 100):
        raise ValueError(' Поле «Возраст» НЕ является числом от 10 до 99' + '\n')
    else:
        for char in sym_email:
            if char not in sym_email:
                raise SyntaxError(' Поле «Имейл» НЕ содержит @ и . (точку)' + '\n')
    return file


with open('registrations.txt', 'r', encoding='utf-8') as users_file:
    for line in users_file:
        line = line[:-1]
        try:
            string = verification_data(line)
        except (IndexError, NameError, SyntaxError, ValueError) as exc:
            error_message = f'{line.rstrip()} - {exc.__class__.__name__} - {exc}\n'
            bad = open('registrations_bad.log', 'a', encoding='utf-8')
            bad.write(error_message)
            bad.close()
        else:
            good = open('registrations_good.log', mode='a', encoding='utf-8')
            good.write(line + '\n')
            good.close()
