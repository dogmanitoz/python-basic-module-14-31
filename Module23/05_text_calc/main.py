import os

with open('calc.txt', 'r') as file:
    print('Содержимое файла', file.name)
    print(file.read() + '\n')

result = []
with open('calc.txt', 'r') as file:
    for line in file.readlines():
        try:
            result.append(eval(line))
        except Exception:
            question = input(f'Обнаружена ошибка в строке: {line[:-1]}   Хотите исправить? ')
            if question.title() == 'Да':
                answer = input('Введите исправленную строку: ')
                result.append(eval(answer))
print('Сумма результатов:', sum(result))
