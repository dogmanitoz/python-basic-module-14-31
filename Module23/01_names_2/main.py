names = ['Василий', 'Николай', 'Надежда', 'Никита', 'Ян', 'Ольга', 'Евгения', 'Кристина']
string = 0
total_sym = 0

try:
    with open('people.txt', 'w', encoding='utf-8') as name_file:
        for i_name in names:
            name_file.write(i_name + '\n')
    with open('people.txt', 'r', encoding='utf-8') as name_file:
        print(f'Содержимое файла {name_file.name}:')
        print(name_file.read())
    with open('people.txt', 'r', encoding='utf-8') as name_file:
        for i_name in name_file:
            for i_sym in i_name:
                if i_sym.isalpha():
                    total_sym += 1
            try:
                if len(i_name[:-1]) < 3:
                    raise ValueError
            except ValueError:
                print(f'Ошибка: менее трёх символов в строке {string}.')
                with open('errors.log', 'w', encoding='utf-8') as error_file:
                    error_file.write(f'Ошибка: менее трёх символов в строке {string}.')
            string += 1
finally:
    print('Общее количество символов:', total_sym)
