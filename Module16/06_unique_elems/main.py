first_list = []
second_list = []
unique_list = []

for i in range(1, 4):
    print(f'Введите {i} число для первого списка: ', end='')
    number = int(input())
    first_list.append(number)

for i in range(1, 8):
    print(f'Введите {i} число для второго списка: ', end='')
    number = int(input())
    second_list.append(number)

print('\nПервый список:', first_list)
print('Второй список:', second_list)
print()

first_list.extend(second_list)
for _ in range(len(first_list)):
    for i in first_list:
        if first_list.count(i) > 1:
            first_list.remove(i)
# for num in first_list:
#     if num not in unique_number:
#         unique_list.append(num)

print('Новый первый список с уникальными элементами:', first_list)
