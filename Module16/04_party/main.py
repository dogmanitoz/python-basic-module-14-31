guests = ['Петя', 'Ваня', 'Саша', 'Лиза', 'Катя']

while True:
    print(f'Сейчас на вечеринке {len(guests)} человек:', guests)
    action = input('Гость пришел или ушел? ')
    if action == 'пора спать':
        print('Вечеринка закончилась, все легли спать.')
        break
    name = input('Имя гостя: ')
    if action == 'пришел':
        if len(guests) < 6:
            guests.append(name.title())
            print(f'Привет, {name}!')
        else:
            print(f'Прости, {name}, но мест нет.')
    elif action == 'ушел':
            guests.remove(name.title())
            print(f'Пока, {name}!')
