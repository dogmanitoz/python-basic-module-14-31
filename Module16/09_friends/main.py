friends = int(input('Кол-во друзей: '))
vouchers = int(input('Долговых расписок: '))
friends_list = [0 for i in range(1, friends + 1)]

for i in range(1, vouchers + 1):
    print(f'\n{i} расписка')
    getting = int(input('Кому: '))
    returning = int(input('От кого: '))
    money = int(input('Сколько: '))
    friends_list[returning - 1] += money
    friends_list[getting - 1] -= money

print('Баланс друзей: ')
for index in range(len(friends_list)):
    print(index + 1, " : ", friends_list[index])
