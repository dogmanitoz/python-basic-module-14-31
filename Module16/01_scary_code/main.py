a = [1, 5, 5, 3]
b = [1, 5, 1, 5]
c = [1, 3, 1, 5, 3, 3]

a.extend(b)
number_five = a.count(5)

for _ in range(number_five):
    a.remove(5)

a.extend(c)
number_three = a.count(3)

print('Кол-во цифр 5 при первом объединении:', number_five)
print('Кол-во цифр 3 при втором объединении:', number_three)
print('Итоговый список:', a)
