skates_size = []
foot_size = []
count = 0

skates_count = int(input('Кол-во коньков: '))
for skate in range(skates_count):
    skates_size.append(int(input(f'Размер {skate + 1} пары: ')))
print()

human_count = int(input('Кол-во людей: '))
for human in range(human_count):
    foot_size.append(int(input(f'Размер ноги {human + 1} человека: ')))

for num in foot_size:
    if num in skates_size:
        skates_size.remove(num)
        count += 1

print('\nНаибольшее кол-во людей, которые могут взять ролики:', count)
