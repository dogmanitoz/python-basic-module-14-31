violator_songs = [
    ['World in My Eyes', 4.86],
    ['Sweetest Perfection', 4.43],
    ['Personal Jesus', 4.56],
    ['Halo', 4.9],
    ['Waiting for the Night', 6.07],
    ['Enjoy the Silence', 4.20],
    ['Policy of Truth', 4.76],
    ['Blue Dress', 4.29],
    ['Clean', 5.83]
]
my_list = []
time_songs = 0
songs_count = int(input('Сколько песен выбрать? '))

for i in range(songs_count):
    print(f'Название {i + 1} песни: ', end='')
    name_song = input()
    my_list.append(name_song)
for song in violator_songs:
    if song[0] in my_list:
        time_songs += song[1]

print('Общее время звучания песен:', round(time_songs, 2),' минут')
