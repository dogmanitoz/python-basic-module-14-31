humans_count = int(input('Кол-во человек: '))
number = int(input('Какое число в считалке? '))
start = 0
print(f'Значит, выбывает каждый {number} человек')

human_list = [i for i in range(1, humans_count + 1)]

while len(human_list) > 1:
    print('\nТекущий круг людей:', human_list)
    print('Начало счёта с номера', human_list[start])
    retired = (start + number - 1) % len(human_list)
    if human_list[retired] == human_list[-1]:
        start = 0
    else:
        start = retired
    print(f'Выбывает человек под номером', human_list.pop(retired))

print('\nОстался человек под номером', human_list[0])
