from abc import ABC
import math


class Figure(ABC):
    """Абстрактный класс Figure"""

    def __init__(self, f_name: str) -> None:
        self.f_name = f_name

    def get_name(self):
        print('Название фигуры- {}'.format(self.f_name))


class Triangle(Figure):
    """Класс Треугольник"""
    def __init__(self, a: int, h: int):
        self.a = a
        self.h = h
        super().__init__('Треугольник')

    def get_name(self):
        super().get_name()

    def get_perimeter(self):
        perimeter = 2 * math.sqrt(self.a ** 2 - self.h ** 2) + 2 * self.a
        return print('Периметр - {}'.format(round(perimeter, 2)))

    def get_square(self):
        square = 0.5 * self.a * self.h
        return print('Площадь - {}'.format(round(square, 2)))


class Square(Figure):
    """Класс Квадрат"""
    def __init__(self, a: int):
        super().__init__('Квадрат')
        self.a = a
        square = 0

    def get_name(self):
        super().get_name()

    def get_perimeter(self):
        perimeter = self.a * 4
        return print('Периметр - {}'.format(round(perimeter, 2)))

    def get_square(self):
        square = self.a ** 2
        return print('Площадь - {}'.format(round(square, 2)))


class Pyramid(Figure):
    """Класс Пирамида"""
    def __init__(self, a: int, b: int, h: int):
        super().__init__('Пирамида')
        self.a = a
        self.b = b
        self.h = h

    def get_name(self):
        super().get_name()

    def get_square(self):
        square_pyramid = (self.a ** 2) + (0.5 * self.b * self.h)
        return print('Площадь - {}'.format(round(square_pyramid, 2)))


class Cube(Figure):
    """Класс Куб"""
    def __init__(self, a: int):
        super().__init__('Куб')
        self.a = a

    def get_name(self):
        super().get_name()

    def get_square(self):
        square_cube = 6 * self.a ** 2
        return print('Площадь - {}'.format(round(square_cube, 2)))


t = Triangle(4, 1)
t.get_name()
t.get_square()
t.get_perimeter()

c = Cube(4)
c.get_name()
c.get_square()

p = Pyramid(1, 2, 3)
p.get_name()
p.get_square()
