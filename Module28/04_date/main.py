class Date:
    def __init__(self, day: int = 0, month: int = 0, year: int = 0) -> None:
        self.day = day
        self.month = month
        self.year = year

    def __str__(self) -> str:
        return 'День: {}\tМесяц: {}\tГод: {}'.format(self.day, self.month, self.year)

    @classmethod
    def parse_date(cls, date):
        return tuple(map(int, date.split('-')))

    @classmethod
    def is_date_valid(cls, date: str) -> bool:
        day, month, year = cls.parse_date(date)
        return 0 < day <= 31 and 0 < month <= 12 and 0 < year

    @classmethod
    def from_string(cls, date: str) -> 'Date':
        day, month, year = cls.parse_date(date)
        date_object = cls(day, month, year)
        return date_object


my_date = Date.from_string('10-12-2077')
print(my_date)
print(Date.is_date_valid('10-12-2077'))
print(Date.is_date_valid('40-12-2077'))
