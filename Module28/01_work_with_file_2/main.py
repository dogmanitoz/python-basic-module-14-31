import os.path


class File:
    """Модернизированная версия контекст-менеджера File"""
    def __init__(self, filename: str, mode: str):
        self.filename = filename
        self.mode = mode
        self.file = None

    def __enter__(self):
        if os.path.isfile(self.filename):
            self.file = open(self.filename, self.mode)
        else:
            self.file = open(self.filename, 'w')
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()
        return True


if __name__ == '__main__':
    with File('example', '+w') as file:
        file.write('Test')
