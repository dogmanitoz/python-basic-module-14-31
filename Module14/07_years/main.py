year_start = int(input("Введите первый год: "))
year_stop = int(input("Введите второй год: "))

print(f'Года от {year_start} до {year_stop} с тремя одинаковыми цифрами:')

# for year in range(year_start, year_stop + 1):
#     num_1, num_2, num_3, num_4 = year // 1000, year // 100 % 10, year // 10 % 10, year % 10
#     if (num_1 == num_2 == num_3) or (num_1 == num_2 == num_4) or (num_2 == num_3 == num_4) or (num_1 == num_3 == num_4):
#         print(year)

# or

year_start = int(input("Введите первый год: "))
year_stop = int(input("Введите второй год: "))
answer = []

for year in range(year_start, year_stop + 1):
    str_year = str(year)
    set_str_year = set(str_year)
    for digit in set_str_year:
        if str_year.count(digit) > 2 and str_year.count(digit) != 4:
            answer.append(year)
print(f'Года от {year_start} до {year_stop} с тремя одинаковыми цифрами:')
print(*answer, sep='\n')
