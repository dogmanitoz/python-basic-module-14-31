def find_coin(k_1, k_2, r):
    if (k_1 ** 2 + k_2 ** 2) ** 0.5 <= r:
        print("Монетка где-то рядом")
    else:
        print("Монетки в области нет")


print("Введите координаты монетки:")
x = float(input("X: "))
y = float(input("Y: "))
radius = int(input("Введите радиус: "))

find_coin(x, y, radius)
