def reverse(number):
    flag = True
    whole = ""
    part = ""
    for symbol in number:
        if symbol == ".":
            flag = False
        elif flag:
            whole = symbol + whole
        else:
            part = symbol + part
    reversed = float(whole + '.' + part)
    return reversed


number_1 = input("Введите первое число: ")
number_2 = input("Введите второе число: ")

reverse_number_1 = reverse(number_1)
reverse_number_2 = reverse(number_2)

summ = reverse_number_1 + reverse_number_2

print("Первое число наоборот:", reverse_number_1)
print("Второе число наоборот:", reverse_number_2)
print("Сумма:", summ)
