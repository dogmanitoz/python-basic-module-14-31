number = int(input("Введите число: "))
count = 0
summ = 0
digitcount = 0

while number != 0:
    count = number % 10
    summ += count
    number //= 10
    digitcount += 1
diff = summ - digitcount

print("Сумма цифр:", summ)
print("Кол-во цифр в числе:", digitcount)
print("Разность суммы и кол-ва цифр:", diff)
