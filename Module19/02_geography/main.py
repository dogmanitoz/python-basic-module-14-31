country_count = int(input('Кол-во стран: '))
country_dict = dict()

for i in range(country_count):
    print(f'{i + 1} страна: ', end='')
    country_list = input().split()
    for city in country_list[1:]:
        country_dict[city] = country_list[0]

for i in range(3):
    town = input('\n1 город: ')
    country = country_dict.get(town)
    if country:
        print('Город {} расположен в стране {}.'.format(town, country))
    else:
        print('По городу {} данных нет.'.format(town))
