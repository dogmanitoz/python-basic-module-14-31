string = input('Введите строку: ')
poly = set()

for i_sym in string:
    if i_sym in poly:
        poly.remove(i_sym)
    else:
        poly.add(i_sym)

if len(poly) == 1:
    print('Можно сделать палиндромом')
else:
    print('Нельзя сделать палиндромом')
