count_words = int(input('Введите количество пар слов: '))
text_dict = dict()

for i in range(count_words):
    pair_dict = input(f'{i + 1} пара: ').lower().split(' - ')
    text_dict[pair_dict[0]] = pair_dict[1]
    text_dict[pair_dict[1]] = pair_dict[0]

while True:
    word = input('Введите слово: ')
    if word in text_dict:
        print('Синоним:', text_dict[word])
    else:
        print('Такого слова в словаре нет.')
