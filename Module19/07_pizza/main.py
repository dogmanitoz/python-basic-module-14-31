orders = int(input('Введите кол-во заказов: '))
orders_dict = dict()

for i in range(orders):
    order = input(f'{i + 1} заказ: ')
    fio, pizza, count = order.split()
    count = int(count)
    if fio not in orders_dict:
        orders_dict[fio] = {pizza: count}
    else:
        if pizza not in orders_dict[fio]:
            orders_dict[fio] |= {pizza: count}
        else:
            orders_dict[fio][pizza] += count
for fio, order in orders_dict.items():
    print(f'{fio}:')
    for pizza, count in order.items():
        print('      ', pizza, ': ', count, sep='')
