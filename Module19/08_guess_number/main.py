max_count = int(input('Введите максимальное число: '))
all_num = set(range(1, max_count + 1))

while True:
    attempt = input('Нужное число есть среди вот этих чисел: ')
    if attempt == 'Помогите!':
        break
    else:
        attempt_num = set(list(map(int, attempt.split())))
        answer = input('Ответ Артёма: ')
        if answer == 'Да':
            all_num &= attempt_num
        else:
            all_num -= attempt_num

print('Артём мог загадать следующие числа:', end=' ')
for i in all_num:
    print(i, end=' ')
