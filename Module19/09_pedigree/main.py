man_count = int(input('Введите количество человек: '))
gen_tree = dict()
tree = dict()

for i in range(man_count - 1):
    man_1, man_2 = input(f'{i +1} пара: ').split()
    tree[man_1] = man_2
    gen_tree[man_1] = 0
    gen_tree[man_2] = 0

for i in tree:
    pair = i
    while pair in tree:
        pair = tree[pair]
        gen_tree[i] += 1

print('\n“Высота” каждого члена семьи:')
for i in sorted(gen_tree):
    print(i, gen_tree[i])
